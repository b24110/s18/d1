//alert("Hi!");
/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function

*/

/*
	Syntax:

		function functionName(parameter) {
				code block
		}
		functionName(argument)

*/

//name is called parameter
//"parameter" acts as a named variable/container that exists only inside of a function
//it is used to store information that is provided to a function when it is called/invoked

let name = "John";

function printName(name) {

	console.log("My Name is " + name);
}


//"Juan" and "Miah", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Miah");

//Variables can also be passed as an ARGUMENT
let userName = "Elaine";

printName(userName);

printName(name);
printName(); // function calling without arguments will result to undefined parameters.

//
function greeting() {
	console.log("Hello, User!");
}

greeting("Eric");

//Using Multiple Parameters
//Multiple "arguments" will corresponds to the number of "parameters" declared in a function in succeeding order.
//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.
//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.


function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " "+ lastName)
}

createFullName("Erven", "Joshua", "Cabral");
//"Erven" will be stored in the parameter "firstName"
//"Joshua" will be stored in the parameter "middleName"
//"Cabral" will be stored in the parameter "lastName"


//In JS, providing more/less arguments than the expected parameters will not return an error
// In other programming languages, this will return an error stating that "the expected number of argument do not match the number of parameters".

createFullName("Eric", "Andales");
createFullName("Roland", "John", "Doroteo", "Jane");

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);